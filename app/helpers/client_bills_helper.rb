module ClientBillsHelper

  def has_access_to_bill
    if is_manager?
      return true
    end
    @client_bill = ClientBill.find(params[:id])
    return current_user.id == @client_bill.client_id
  end

end

module BillOperationsHelper

  def has_access_to_bill_operation
    if is_manager?
      return true
    end
    @bill_operation = BillOperation.find(params[:id])
    return current_user.id == ClientBill.find(@bill_operation.cliend_bill_id).client_id
  end
  
end

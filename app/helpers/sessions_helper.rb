module SessionsHelper
  include ApplicationHelper

  def sign_in(user)
    remember_token = User.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    user.update_attribute(:remember_token, User.encrypt(remember_token))
    self.current_user = user
  end

  def signed_in_user
    if !signed_in?
      flash[:error] = "Please sign in!"
      redirect_to '/signin'
    end
  end
  def signed_in?
    !current_user.nil?
  end

  def correct_user
    if is_manager?
      return true
    end
    @client = Client.find(params[:id])
    @user = User.find_by_client_id(@client.id)
    if !current_user?(@user)
      @client = Client.find(current_user.client_id)
      redirect_to(@client)
    end
  end

  def correct_manager
    if is_admin?
      return true
    end
    @user = User.find(params[:id])
    redirect_to(current_user) unless current_user?(@user)
  end

  def signed_as_manager
    redirect_to('') unless is_manager?
  end
  def is_manager?
    if current_user
     return current_user.status > 0
   else
     return false
   end
  end

  def signed_as_admin
    redirect_to('') unless is_admin?
  end
  def is_admin?
    if current_user
      return current_user.status > 1
    else
      return false
    end
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    remember_token = User.encrypt(cookies[:remember_token])
    @current_user ||= User.find_by(remember_token: remember_token)
  end

  def current_user?(user)
    user == current_user
  end

  def sign_out
    current_user.update_attribute(:remember_token, User.encrypt(User.new_remember_token))
    cookies.delete(:remember_token)
    self.current_user = nil
  end

end

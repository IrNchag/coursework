module CreditOperationsHelper

  def has_access_to_credit_operation
    if is_manager?
      return true
    end
    @credit_operation = CreditOperation.find(params[:id])
    return current_user.id == ClientCredit.find(@credit_operation.cliend_credit_id).client_id
  end
  
end

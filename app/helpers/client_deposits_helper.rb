module ClientDepositsHelper
  
  def has_access_to_deposit
    if is_manager?
      return true
    end
    @client_deposit = ClientDeposit.find(params[:id])
    return current_user.id == @client_deposit.client_id
  end

end

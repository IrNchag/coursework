module ClientCreditsHelper

  def has_access_to_credit
    if is_manager?
      return true
    end
    @client_credit = ClientCredit.find(params[:id])
    return current_user.id == @client_credit.client_id
  end

end

class ClientDeposit < ApplicationRecord
  belongs_to :client
  has_many :deposit_operations, dependent: :destroy
  belongs_to :deposit

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/
  validates :sum, presence: true,
  format: { with: VALID_SUM_REGEX }

  validate :check_deposit
  validate :check_client
  private
  def check_deposit
	errors.add(:deposit_id, "can't find deposit") unless Deposit.exists?(self.deposit_id)
  end

  def check_client
  	errors.add(:client_id, "can't find client") unless Client.exists?(self.client_id)
  end

end

class CreditOperation < ApplicationRecord
  belongs_to :client_credit

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0),[0-9]{2}\z/	
  validates :sum, presence: true,
  format: { with VALID_SUM_REGEX }

  validate :check_client_credit
  private
  def check_client_credit
	errors.add(:client_credit_id, "can't find client's credit") unless ClientCredit.exists?(self.client_credit_id)
  end
end

class Client < ApplicationRecord
  has_many :client_bills, dependent: :destroy
  has_many :client_credits, dependent: :destroy
  has_many :client_deposits, dependent: :destroy
  before_save {
    email.downcase!
    passport.upcase!
  }
  validates :full_name,  presence: true, length: { maximum: 50 }

  VALID_PHONE_REGEX = /\A\+([\d]){12}+\z/ # "+380123456789"
  validates :phone, presence: true, length: { maximum: 20 }, uniqueness: true,
  format: { with: VALID_PHONE_REGEX }

  VALID_PASSPORT_REGEX = /\A([A-Za-z]){2}([\d]){6}+\z/ # "AA202020"
  validates :passport, presence: true, length: { is: 8 },
  format: { with: VALID_PASSPORT_REGEX }
  VALID_ID_NUMBER_REGEX = /\A([\d]){10}\z/
  validates :id_number, presence: true, length: { is: 10 },
  format: { with: VALID_ID_NUMBER_REGEX }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i  # Email validates "abc@ukr.net"
  validates :email, presence: true, length: { maximum: 20 },
  format: { with: VALID_EMAIL_REGEX }

  validates  :address, presence: true, length: { maximum: 50 }

end

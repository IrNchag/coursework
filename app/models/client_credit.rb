class ClientCredit < ApplicationRecord
  belongs_to :client
  has_many :credit_operations, dependent: :destroy
  belongs_to :credit

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/
  validates :sum, presence: true, numericality: true,
  format: { with: VALID_SUM_REGEX }

  validate :check_credit
  validate :check_client
  private
  def check_credit
  errors.add(:credit_id, "can't find credit") unless Credit.exists?(self.credit_id)
  end

  def check_client
    errors.add(:client_id, "can't find client") unless Client.exists?(self.client_id)
  end

end

class DepositOperation < ApplicationRecord
  belongs_to :client_deposit

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/	
  validates :sum, presence: true,
  format: { with VALID_SUM_REGEX }


  validate :check_client_deposit
  private
  def check_client_deposit
	errors.add(:client_deposit_id, "can't find client's deposit") unless ClientDeposit.exists?(self.client_deposit_id)
  end
  
end

class BillOperation < ApplicationRecord
  belongs_to :client_bill
  
  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/	
  validates :sum, presence: true,
  format: { with: VALID_SUM_REGEX }

  validate :check_client_bill
  private
  def check_client_bill
	errors.add(:client_bill_id, "can't find client's bill") unless ClientBill.exists?(self.client_bill_id)
  end

end

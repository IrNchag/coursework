class Deposit < ApplicationRecord
  belongs_to :currency
  
  validates :name,  presence: true, uniqueness: true, length: { maximum: 50 }

  validates :term, numericality: { only_integer: true }
  
  VALID_RATE_REGEX = /\A(([1-9]{1}|[1-9]{1}[0-9]{1}|0).[0-9]{1,2}|100,00)\z/ # 0,12 9,12, 16,00, 100,00 (%)
  validates :rate, presence: true,
  format: { with: VALID_RATE_REGEX }
  #
  validates :widthdrawal, inclusion: { in: [true, false] }
  #
  validates :refill, inclusion: { in: [true, false] }

  validate :check_currency
  private
  def check_currency
    errors.add(:currency_id, "can't find currency") unless Currency.exists?(self.currency_id)
  end
  
end

class User < ApplicationRecord
  before_create :create_remember_token
  has_secure_password
  validates :login, uniqueness: true
  VALID_PASSWORD_REGEX = /\A[\w]+\z/
  validates :password, :confirmation => true, presence: true, length: { in: 6..20 },
  format: { with: VALID_PASSWORD_REGEX }
  validates :password_confirmation, :presence => true

    def User.new_remember_token
      SecureRandom.urlsafe_base64
    end
    def User.encrypt(token)
      Digest::SHA1.hexdigest(token.to_s)
    end
    private
      def create_remember_token
        self.remember_token = User.encrypt(User.new_remember_token)
      end
end

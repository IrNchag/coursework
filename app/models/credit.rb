class Credit < ApplicationRecord
  belongs_to :currency

  validates :name,  presence: true, uniqueness: true, length: { maximum: 50 }

  VALID_MAX_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/
  validates :max_sum, presence: true,
  format: { with: VALID_SUM_REGEX }

  validates :term, numericality: { only_integer: true }

  VALID_RATE_REGEX = /\A(([1-9]{1}|[1-9]{1}[0-9]{1}|0).[0-9]{1,2}|100,00)\z/ # 0,12 9,12, 16,00, 100,00 (%)
  validates :rate, presence: true,
  format: { with: VALID_RATE_REGEX }

  
  validate :check_currency
  private
  def check_currency
    errors.add(:currency_id, "can't find currency") unless Currency.exists?(self.currency_id)
  end

end


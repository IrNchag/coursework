class ClientBill < ApplicationRecord
  belongs_to :client
  belongs_to :currency
  has_many :bill_operations, dependent: :destroy

  VALID_SUM_REGEX = /\A([1-9]{1}[0-9]*|0).[0-9]{1,2}\z/
  validates :sum, presence: true,
  format: { with: VALID_SUM_REGEX }

  validate :check_currency
  validate :check_client
  private
  def check_currency
  	errors.add(:currency_id, "can't find currency") unless Currency.exists?(self.currency_id)
  end

  def check_client
  	errors.add(:client_id, "can't find client") unless Client.exists?(self.client_id)
  end

end

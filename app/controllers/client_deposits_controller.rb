class ClientDepositsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_deposit, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @client_deposit = ClientDeposit.all
  end

  def show
    @client_deposit = ClientDeposit.where(:id => params[:id])
    @deposit_operation = DepositOperation.where(:client_deposit_id => params[:id])
  end

  def create
    client = Client.find(params[:client_id])
    @client_deposit = client.client_deposits.build(client_deposit_params)
    @client_deposit.status = true
    if @client_deposit.save
      flash[:success] = "Successfully added!"
      redirect_to @client_deposit
    else
      render 'new_deposit'
    end
  end

  def edit
    render 'edit'
  end

  def update
    @client_deposit = ClientDeposit.find(params[:id])
    if @client_deposit.update(client_deposit_params)
      flash[:success] = "Successfully updated!"
      redirect_to @client_deposit
    else
      render 'edit'
    end
  end

  def to_archive
    @client_deposit = ClientDeposit.find(params[:id])
    @client_deposit.update(status: false)
    flash[:success] = "Added to archive"
    redirect_to "/clients/#{@client_deposit.client_id}/deposits"
  end

  def new_transaction
    @deposit_operation = DepositOperation.new
  end

  private
    def client_deposit_params
      params.require(:client_deposit).permit(:client_id, :deposit_id, :sum)
    end

    # def deposit_operation_params
    #   params.require(:deposit_operation).permit(:sum)
    # end

end

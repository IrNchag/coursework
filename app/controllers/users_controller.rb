class UsersController < ApplicationController
  before_action :signed_in_user
  before_action :signed_as_admin, except: [:show]
  before_action :correct_manager

  def index
    @user = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    @user.status = 1;
    if @user.save
      flash[:success] = "Successfully added!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      flash[:success] = "Successfully updated!"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:success] = "Successfully deleted!"
    redirect_to '/users'
  end

  def user_params
    params.require(:user).permit(:login, :password, :password_confirmation)
  end

end

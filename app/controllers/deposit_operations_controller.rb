class DepositOperationsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_deposit_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @deposit_operation = DepositOperation.all
  end

  def show
    @deposit_operation = DepositOperation.where(:id => params[:id])
  end

  def create
    @deposit_operation = DepositOperation.new(deposit_operation_params)
    if @deposit_operation.save
      flash[:success] = "Successfully added!"
      redirect_to ClientDeposit.find(params[:id])
    else
      render 'new_transaction'
    end
  end

  private
    def deposit_operation_params
      params.require(:deposit_operation).permit(:client_deposit_id, :sum)
    end

end

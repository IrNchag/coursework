class BillOperationsController < ApplicationController
  before_action :signed_in_bill_operation
  before_action :has_access_to_bill_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @bill_operation = BillOperation.all
  end

  def show
    @bill_operation = BillOperation.where(:id => params[:id])
  end

  def create
    @bill_operation = BillOperation.new(bill_operation_params)
    if @bill_operation.save
      flash[:success] = "Successfully added!"
      redirect_to ClientBill.find(params[:id])
    else
      render 'new_transaction'
    end
  end

  private
  def bill_operation_params
    params.require(:bill_operation).permit(:client_bill_id, :sum)
  end
  
end

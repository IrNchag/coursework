class ClientCreditsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_credit, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @client_credit = ClientCredit.all
  end

  def show
    @client_credit = ClientCredit.where(:id => params[:id])
    @credit_operation = CreditOperation.where(:client_credit_id => params[:id])
  end

  def create
    client = Client.find(params[:client_id])
    @client_credit = client.client_credits.build(client_credit_params)
    @client_credit.status = true
    # Monthly payment ??? payment day ???
    if @client_credit.save
      flash[:success] = "Successfully added!"
      redirect_to @client_credit
    else
      render 'new_credit'
    end
  end

  def edit
    render 'edit'
  end

  def update
    @client_credit = ClientCredit.find(params[:id])
    if @client_credit.update(client_credit_params)
      flash[:success] = "Successfully updated!"
      redirect_to @client_credit
    else
      render 'edit'
    end
  end

  def to_archive
    @client_credit = ClientCredit.find(params[:id])
    @client_credit.update(status: false)
    flash[:success] = "Added to archive"
    redirect_to "/clients/#{@client_credit.client_id}/credits"
  end

  def new_transaction
    @credit_operation = CreditOperation.new
  end

  private
    def client_credit_params
      params.require(:client_credit).permit(:client_id, :credit_id, :sum)
    end

    # def credit_operation_params
    #   params.require(:credit_operation).permit(:sum)
    # end
    
end

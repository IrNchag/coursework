class ClientsController < ApplicationController
  before_action :signed_in_user
  before_action :signed_as_manager, except: [:show, :deposits, :credits, :bills,
     :credit_transactions, :deposit_transactions, :bill_transactions, :transactions,
   :archive_deposits, :archive_credits, :archive_bills]
  before_action :correct_user

  def index
    if params[:q] == nil
      @client = Client.all
    elsif (Integer (params[:q]) rescue nil) != nil
      @client = Client.where('id_number = ?', params[:q])
    elsif (Integer (params[:q][2]) rescue nil) != nil
      @client = Client.where('passport = ?', params[:q])
    else
      @client = Client.where('full_name = ?', params[:q])
    end
  end

  def show
    @client = Client.find(params[:id])
  end

  def deposits
    @client_deposit = ClientDeposit.where('client_id=? AND status=?', params[:id], true)
  end

  def credits
    @client_credit = ClientCredit.where('client_id=? AND status=?', params[:id], true)
  end

  def bills
    @client_bill = ClientBill.where('client_id=? AND status=?', params[:id], true)
  end

  def archive_deposits
    @archive_client_deposit = ClientDeposit.where('client_id=? AND status=?', params[:id], false)
  end

  def archive_credits
    @archive_client_credit = ClientCredit.where('client_id=? AND status=?', params[:id], false)
  end

  def archive_bills
    @archive_client_bill = ClientBill.where('client_id=? AND status=?', params[:id], false)
  end

  def credit_transactions
  	@credit_transaction = CreditOperation.where('client_credit_id = ? ', params[:id])
  end

  def deposit_transactions
  	@deposit_transaction = DepositOperation.where('client_deposit_id = ?', params[:id])
  end

  def bill_transactions
  	@bill_transaction = BillOperation.where('client_bill_id = ?', params[:id])
  end

  def transactions
  	credit_t = CreditOperation.where('client_credit_id = ? ', params[:id])
  	deposit_t = DepositOperation.where('client_deposit_id = ?', params[:id])
  	bill_t = BillOperation.where('client_bill_id = ?', params[:id])
  	@transaction = credit_t + deposit_t + bill_t
  	@transaction ||= []
  	@transaction.sort! { |x, y| y[:created_at] <=> x[:created_at]}
  end

  def new_deposit
    @client_deposit = ClientDeposit.new
  end

  def new_credit
    @client_credit = ClientCredit.new
  end

  def new_bill
    @client_bill = ClientBill.new
  end

  def new
    @client = Client.new
    @user = User.new
  end

  def edit
    @client = Client.find(params[:id])
    @user = User.find_by_client_id(params[:id])
  end

  def create
    @client = Client.new(client_params)
    @user = User.new(params.require(:user).permit(:password, :password_confirmation))
    @user.login = @client.phone
    @user.status = 0
    if !@client.valid? || !@user.valid?
      @user.valid?
      render 'new'
    else
      @client.save
      @user.client_id = @client.id
      @user.save
      flash[:success] = "Successfully added!"
      redirect_to @client
    end
  end

  def update
    @client = Client.find(params[:id])
    if @client.update(client_params)
      flash[:success] = "Successfully updated!"
      redirect_to @client
    else
      render 'edit'
    end
  end

  def destroy
    @client = Client.find(params[:id])
    @user = User.find_by_client_id(@client.id)
    if @client && @user
      @client.destroy
      @user.destroy
      flash[:success] = "Successfully deleted!"
    end
    redirect_to ''
  end

  private

    # def client_deposit_params
    #   params.require(:client_deposit).permit(:deposit_id, :sum)
    # end
    #
    # def client_credit_params
    #   params.require(:client_credit).permit(:credit_id, :sum)
    # end
    #
    # def client_bill_params
    #   params.require(:client_bill).permit(:currency_id, :sum)
    # end

    def client_params
      params.require(:client).permit(:full_name, :phone, :passport, :id_number, :email, :address, :birth, :sex)
    end

end

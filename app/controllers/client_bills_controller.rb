class ClientBillsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_bill, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @client_bill = ClientBill.all
  end

  def show
    @client_bill = ClientBill.where(:id => params[:id])
    @bill_operation = BillOperation.where(:client_bill_id => params[:id])
  end

  def create
    client = Client.find(params[:client_id])
    @client_bill = client.client_bills.build(client_bill_params)
    @client_bill.status = true
    if @client_bill.save
      flash[:success] = "Successfully added!" 
      redirect_to @client_bill
    else
      render 'new_bill'
    end
  end

  def edit
    render 'edit'
  end

  def update
    @client_bill = ClientBill.find(params[:id])
    if @client_bill.update(client_bill_params)
      flash[:success] = "Successfully updated!"
      redirect_to @client_bill
    else
      render 'edit'
    end
  end

  def to_archive
    @client_bill = ClientBill.find(params[:id])
    @client_bill.update(status: false)
    flash[:success] = "Added to archive"
    redirect_to "/clients/#{@client_bill.client_id}/bills"
  end

  def new_transaction
    @bill_operation = BillOperation.new
  end

  private
    def client_bill_params
      params.require(:client_bill).permit(:client_id, :currency_id, :sum)
    end

    # def bill_operation_params
    #   params.require(:bill_operation).permit(:sum)
    # end

end

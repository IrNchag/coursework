class DepositsController < ApplicationController
  before_action :signed_as_manager, except: [:index, :show]

  def index
    @deposit = Deposit.all
  end

  def show
    @deposit = Deposit.find(params[:id])
  end

  def new
    @deposit = Deposit.new
  end

  def create
    @deposit = Deposit.new(deposit_params)
    if @deposit.save
      flash[:success] = "Successfully added!"
      redirect_to '/deposits'
    else
      render 'new'
    end
  end

  def edit
    @deposit = Deposit.find(params[:id])
  end
  def update
    @deposit = Deposit.find(params[:id])
    if @deposit.update(deposit_params)
      flash[:success] = "Successfully updated!"
      redirect_to '/deposits'
    else
      render 'edit'
    end
  end

  def destroy
    @deposit = Deposit.find(params[:id])
    @deposit.destroy
    flash[:success] = "Successfully deleted!"
    redirect_to '/deposits'
  end

  private
    def deposit_params
      params.require(:deposit).permit(:name, :currency_id, :term, :rate, :widthdrawal, :refill)
    end

end

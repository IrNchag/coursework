class CreditOperationsController < ApplicationController
  before_action :signed_in_user
  before_action :has_access_to_credit_operation, only: [:show]
  before_action :signed_as_manager, except: [:show]

  def index
      @credit_operation = CreditOperation.all
  end

  def show
    @credit_operation = CreditOperation.where(:id => params[:id])
  end

  def create
    @credit_operation = CreditOperation.new(credit_operation_params)
    if @credit_operation.save
      flash[:success] = "Successfully added!"
      redirect_to ClientCredit.find(params[:id])
    else
      render 'new_transaction'
    end
  end

  private
    def credit_operation_params
      params.require(:credit_operation).permit(:client_credit_id, :sum)
    end

end

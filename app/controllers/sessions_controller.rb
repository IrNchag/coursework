class SessionsController < ApplicationController

  def index
  end

  def new
  end

  def create
    user = User.find_by(login: params[:session][:login])
    if user && user.authenticate(params[:session][:password])
      sign_in(user)
      flash[:success] = "Successfully signed in!"
      redirect_to ''
    else
      flash.now[:error] = "Invalid login/password combination!"
      render 'new'
    end
  end

  def search
  end

  def destroy
    sign_out
    flash[:notice] = "Signed out!"
    redirect_to ''
  end

end

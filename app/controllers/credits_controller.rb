class CreditsController < ApplicationController
  before_action :signed_as_manager, except: [:index, :show]

  def index
    @credit = Credit.all
  end

  def show
    @credit = Credit.find(params[:id])
  end

  def new
    @credit = Credit.new
  end

  def create
    @credit = Credit.new(credit_params)
    if @credit.save
      flash[:success] = "Successfully added!"
      redirect_to '/credits'
    else
      render 'new'
    end
  end

  def edit
    @credit = Credit.find(params[:id])
  end
  def update
    @credit = Credit.find(params[:id])
    if @credit.update(credit_params)
      flash[:success] = "Successfully updated!"
      redirect_to '/credits'
    else
      render 'edit'
    end
  end

  def destroy
    @credit = Credit.find(params[:id])
    @credit.destroy
    flash[:success] = "Successfully deleted!"
    redirect_to '/credits'
  end

  private
    def credit_params
      params.require(:credit).permit(:name, :currency_id, :term, :rate, :max_sum, :penalty)
    end

end

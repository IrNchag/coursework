class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :full_name
      t.string :phone, unique: true
      t.string :passport
      t.string :id_number
      t.string :email
      t.string :address
      t.date :birth
      t.boolean :sex

      t.timestamps
    end
    add_index :clients, :full_name
    add_index :clients, :passport, unique: true
    add_index :clients, :id_number, unique: true
  end
end

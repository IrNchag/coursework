class CreateCreditOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :credit_operations do |t|
      t.integer :client_credit_id
      t.float :sum

      t.timestamps
    end
    add_index :credit_operations, :client_credit_id
  end
end

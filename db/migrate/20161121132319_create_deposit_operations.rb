class CreateDepositOperations < ActiveRecord::Migration[5.0]
  def change
    create_table :deposit_operations do |t|
      t.integer :client_deposit_id
      t.float :sum

      t.timestamps
    end
    add_index :deposit_operations, :client_deposit_id
  end
end

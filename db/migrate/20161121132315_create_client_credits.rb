class CreateClientCredits < ActiveRecord::Migration[5.0]
  def change
    create_table :client_credits do |t|
      t.integer :client_id
      t.integer :credit_id
      t.float :sum
      t.float :monthly_payment
      t.date :payment_date
      t.boolean :status

      t.timestamps
    end
    add_index :client_credits, :client_id
  end
end

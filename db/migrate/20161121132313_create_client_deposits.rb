class CreateClientDeposits < ActiveRecord::Migration[5.0]
  def change
    create_table :client_deposits do |t|
      t.integer :client_id
      t.integer :deposit_id
      t.float :sum
      t.boolean :status

      t.timestamps
    end
    add_index :client_deposits, :client_id
  end
end

CREATE TABLE "schema_migrations" ("version" varchar NOT NULL PRIMARY KEY);
CREATE TABLE "ar_internal_metadata" ("key" varchar NOT NULL PRIMARY KEY, "value" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "clients" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "full_name" varchar, "phone" varchar, "passport" varchar, "id_number" varchar, "email" varchar, "address" varchar, "birth" date, "sex" boolean, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_clients_on_full_name" ON "clients" ("full_name");
CREATE UNIQUE INDEX "index_clients_on_passport" ON "clients" ("passport");
CREATE UNIQUE INDEX "index_clients_on_id_number" ON "clients" ("id_number");
CREATE TABLE "client_bills" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_id" integer, "sum" float, "currency_id" integer, "status" boolean, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "client_deposits" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_id" integer, "deposit_id" integer, "sum" float, "status" boolean, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_client_deposits_on_client_id" ON "client_deposits" ("client_id");
CREATE TABLE "client_credits" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_id" integer, "credit_id" integer, "sum" float, "monthly_payment" float, "payment_date" date, "status" boolean, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_client_credits_on_client_id" ON "client_credits" ("client_id");
CREATE TABLE "bill_operations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_bill_id" integer, "sum" float, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_bill_operations_on_client_bill_id" ON "bill_operations" ("client_bill_id");
CREATE TABLE "deposit_operations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_deposit_id" integer, "sum" float, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_deposit_operations_on_client_deposit_id" ON "deposit_operations" ("client_deposit_id");
CREATE TABLE "credit_operations" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "client_credit_id" integer, "sum" float, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_credit_operations_on_client_credit_id" ON "credit_operations" ("client_credit_id");
CREATE TABLE "deposits" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "currency_id" integer, "term" integer, "rate" float, "widthdrawal" boolean, "refill" boolean, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "credits" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "currency_id" integer, "term" integer, "rate" float, "max_sum" float, "penalty" float, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "currencies" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "symbol" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "login" varchar, "password_digest" varchar, "remember_token" varchar, "client_id" integer, "status" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE UNIQUE INDEX "index_users_on_login" ON "users" ("login");
CREATE UNIQUE INDEX "index_users_on_client_id" ON "users" ("client_id");
INSERT INTO schema_migrations (version) VALUES ('20161121132310'), ('20161121132312'), ('20161121132313'), ('20161121132315'), ('20161121132317'), ('20161121132319'), ('20161121132321'), ('20161121132322'), ('20161121132324'), ('20161121132326'), ('20161204124355');


